#with CID
ab -n 100 -c 10 -H 'Host: mockbin.com' http://localhost:8000/bin/b76eee69-1608-4593-9a8a-740850ec0770/
This is ApacheBench, Version 2.3 <$Revision: 1430300 $>
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/
Licensed to The Apache Software Foundation, http://www.apache.org/

Benchmarking localhost (be patient).....done


Server Software:        cloudflare-nginx
Server Hostname:        localhost
Server Port:            8000

Document Path:          /bin/b76eee69-1608-4593-9a8a-740850ec0770/
Document Length:        0 bytes

Concurrency Level:      10
Time taken for tests:   0.974 seconds
Complete requests:      100
Failed requests:        0
Write errors:           0
Total transferred:      52609 bytes
HTML transferred:       0 bytes
Requests per second:    102.65 [#/sec] (mean)
Time per request:       97.422 [ms] (mean)
Time per request:       9.742 [ms] (mean, across all concurrent requests)
Transfer rate:          52.74 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        0    0   0.1      0       0
Processing:    51   74  27.9     67     215
Waiting:       51   74  27.9     67     214
Total:         51   75  28.0     68     215

Percentage of the requests served within a certain time (ms)
  50%     68
  66%     77
  75%     80
  80%     87
  90%     97
  95%    136
  98%    195
  99%    215
 100%    215 (longest request)
[vagrant@localhost ~]$ ab -n 1000 -c 100 -H 'Host: mockbin.com' http://localhost:8000/bin/b76eee69-1608-4593-9a8a-740850ec0770/
This is ApacheBench, Version 2.3 <$Revision: 1430300 $>
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/
Licensed to The Apache Software Foundation, http://www.apache.org/

Benchmarking localhost (be patient)
Completed 100 requests
Completed 200 requests
Completed 300 requests
Completed 400 requests
Completed 500 requests
Completed 600 requests
Completed 700 requests
Completed 800 requests
Completed 900 requests
Completed 1000 requests
Finished 1000 requests


Server Software:        cloudflare-nginx
Server Hostname:        localhost
Server Port:            8000

Document Path:          /bin/b76eee69-1608-4593-9a8a-740850ec0770/
Document Length:        0 bytes

Concurrency Level:      100
Time taken for tests:   10.504 seconds
Complete requests:      1000
Failed requests:        0
Write errors:           0
Total transferred:      526583 bytes
HTML transferred:       0 bytes
Requests per second:    95.20 [#/sec] (mean)
Time per request:       1050.413 [ms] (mean)
Time per request:       10.504 [ms] (mean, across all concurrent requests)
Transfer rate:          48.96 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        0    0   0.4      0       2
Processing:    50  899 1711.8     67    8092
Waiting:       50  899 1711.8     67    8092
Total:         50  899 1711.8     67    8092

Percentage of the requests served within a certain time (ms)
  50%     67
  66%    115
  75%   1169
  80%   1224
  90%   3508
  95%   4711
  98%   5848
  99%   8008
 100%   8092 (longest request)




##with CID and auth key
ab -n 1000 -c 100 -H 'Host: mockbin.com' -H 'apikey: a75ecaa7fba84eb495909255db7cd51a' http://localhost:8000/bin/b76eee69-1608-4593-9a8a-740850ec0770/
This is ApacheBench, Version 2.3 <$Revision: 1430300 $>
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/
Licensed to The Apache Software Foundation, http://www.apache.org/

Benchmarking localhost (be patient)
Completed 100 requests
Completed 200 requests
Completed 300 requests
Completed 400 requests
Completed 500 requests
Completed 600 requests
Completed 700 requests
Completed 800 requests
Completed 900 requests
Completed 1000 requests
Finished 1000 requests


Server Software:        cloudflare-nginx
Server Hostname:        localhost
Server Port:            8000

Document Path:          /bin/b76eee69-1608-4593-9a8a-740850ec0770/
Document Length:        0 bytes

Concurrency Level:      100
Time taken for tests:   10.335 seconds
Complete requests:      1000
Failed requests:        0
Write errors:           0
Total transferred:      526580 bytes
HTML transferred:       0 bytes
Requests per second:    96.76 [#/sec] (mean)
Time per request:       1033.527 [ms] (mean)
Time per request:       10.335 [ms] (mean, across all concurrent requests)
Transfer rate:          49.76 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        0    0   0.5      0       3
Processing:    50  903 1872.6     65    8280
Waiting:       50  903 1872.6     65    8280
Total:         50  904 1872.6     65    8280

Percentage of the requests served within a certain time (ms)
  50%     65
  66%    121
  75%    216
  80%   1223
  90%   3519
  95%   4739
  98%   8168
  99%   8190
 100%   8280 (longest request)
