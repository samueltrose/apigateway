Feature: Printsites api
Scenario: *** Base Gateway Test ***

    Given address "https://api.mydomain.com"

    Given URL path "/"

    Given format "JSON"

    When the URL is invoked

    Then response is equal to "{"request_path":"\/","message":"API not found with these values","request_host":["api.mydomain.com"]}"

    And status is "404"
Scenario: *** XSS Attack WAF Test ***

    Given address "https://api.mydomain.com"

    Given query string "?name%3Dguest%3Cscript%3Ealert%27attacked%27%3C%2Fscript%3E"

    When the URL is invoked

    Then status is "404"

Scenario: *** 404 WAF Test ***

    Given address "https://api.mydomain.com"

    Given format "JSON"

    Given URL path "/v1"

    When the URL is invoked

    Then status is "404"



Scenario: Keystone Health

    Given address "https://api.mydomain.com"

    Given format "JSON"

    Given URL path "/health/keystone"

    When the URL is invoked

    Then status is "200"
