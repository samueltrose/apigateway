package = "CustomPluginDeps"
version = "1.0-1"
source = {
   url = "..." -- We don't have one yet
}
description = {
   summary = "just adding deps for custom plugins",
}
dependencies = {
   "stringy ~> 0.4-1"
   -- If you depend on other rocks, add them here
}
build = {
   type = "builtin"
}
