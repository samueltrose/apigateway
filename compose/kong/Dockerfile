FROM debian:jessie
MAINTAINER Sam Rose, srose@mydomain.com

ENV KONG_VERSION 0.10.1

RUN apt-get update && apt-get install -y \
    curl \
    wget \
    netcat \
    openssl \
    libpcre3 \
    dnsmasq \
    procps \
    perl \
    unzip \
    gcc \
    g++ \
    git \
    net-tools \
    telnet \
    vim

RUN wget https://github.com/Mashape/kong/releases/download/$KONG_VERSION/kong-$KONG_VERSION.jessie_all.deb && \
dpkg -i kong-$KONG_VERSION.*.deb

RUN wget -O /usr/local/bin/dumb-init https://github.com/Yelp/dumb-init/releases/download/v1.1.3/dumb-init_1.1.3_amd64 && \
    chmod +x /usr/local/bin/dumb-init

COPY entrypoint.sh /entrypoint.sh
COPY custom_nginx.template /etc/kong/custom_nginx.template

# I'm not sure why, but I had problems here loading the kong conf.
# It seemed like at least one command wanted the kong.conf in /etc/kong.conf
# and another wanted it in /etc/kong/kong.conf. Or maybe it was late...
# either way, the kong cli should work, and this makes it work.
COPY kong.conf /etc/kong.conf
COPY kong.conf /etc/kong/kong.conf
COPY scripts /scripts

RUN chmod +x /entrypoint.sh \
    && kong check /etc/kong.conf

WORKDIR /

EXPOSE 8000 8443 8001 7946

ENTRYPOINT ["/entrypoint.sh"]