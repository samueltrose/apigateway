#!/bin/bash
set -e
cmd="$@"


#
# Disabling nginx daemon mode
export KONG_NGINX_DAEMON="off"
[ -z "$KONG_NGINX_DAEMON" ] && export KONG_NGINX_DAEMON="off"

#
# Doing some serious stuff here.
# Due to human knowledge limitations most likely, I am unable to figure out how exactly to set
# env vars via this script. so we get some sed action up in here and set them in the conf files.
#THIS_CONTAINER_ID_LONG=`cat /proc/self/cgroup | grep 'docker' | sed 's/^.*\///' | tail -n1`
#THIS_CONTAINER_ID_SHORT=${THIS_CONTAINER_ID_LONG:0:12}
#THIS_DOCKER_CONTAINER_IP_LINE=`cat /etc/hosts | grep $THIS_CONTAINER_ID_SHORT`
#THIS_DOCKER_CONTAINER_IP=`(echo $THIS_DOCKER_CONTAINER_IP_LINE | grep -o '[0-9]\+[.][0-9]\+[.][0-9]\+[.][0-9]\+')`
#echo "Container IP = $THIS_DOCKER_CONTAINER_IP"
#export KONG_CLUSTER_ADVERTISE="$THIS_DOCKER_CONTAINER_IP:7946"
#sed -i 's/1.2.3.4:5555/'"$KONG_CLUSTER_ADVERTISE"'/g' /etc/kong.conf
#sed -i 's/1.2.3.4:5555/'"$KONG_CLUSTER_ADVERTISE"'/g' /etc/kong/kong.conf

exec $cmd

