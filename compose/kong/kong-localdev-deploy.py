import requests
import json
import time


def populate_api(url, payload):
    r = requests.post(url, data=payload)
    print r.text
    return r


def test_created_api(url, payload):
    t = requests.get(url, headers=payload)
    print "TEST POPULATED API" + t.text


apis = json.load(open('localdev.json'))


for api in apis['data']:
    print api['request_host']
    api_data = {
        'name': api['name'],
        'upstream_url': api['upstream_url'],
        'request_host': api['request_host'],
    }
    host = str(api['request_host'])
    test_data = {
        'Host': host,
    }
    populate_api('http://localhost:8001/apis/', api_data)
    time.sleep(25)
    test_created_api('http://localhost:8000/', test_data)
