#!/bin/bash

#
# This script is really bad, so basically we need to dynamically generate the .env file for tests prior to running the
# Docker commands.
# Problems:
#  1) Process of greping the entire env misses some vars
#  2) If the grep fails to find anything it returns back exit code 1 resulting in a failed build. but fails silently and no easy way to debug.
#  3) Not sure if just env piping the entire env to .env would result in something going haywire or not. (should probably just test)

#run compose build
docker-compose build
